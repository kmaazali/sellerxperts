<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewEggOrders extends Model
{
    //  
    protected $table='newegg_orders';

    protected $fillable=['SellerOrderNumber',
        'CustomerPONumber',
        'SellerCustomerNumber',
        'OrderDate',
        'OrderStatusDescription',
        'CustomerName',
        'CustomerPhoneNumber',
        'CustomerEmailAddress',
        'ShipToAddress1',
        'ShipToAddress2',
        'ShipToCityName',
        'ShipToStateCode',
        'ShipToZipCode',
        'ShipToCountryCode',
        'ShipService',
        'ShipToFirstName',
        'ShipToLastName',
        'ShipToCompany',
        'OrderItemAmount',
        'ShippingAmount',
        'DiscountAmount',
        'RefundAmount',
        'SalesTax',
        'VATTotal',
        'DutyTotal',
        'OrderTotalAmount',
        'OrderQty',
        'order_status_id'];

        public function orderItem(){
            return $this->belongsTo('App\NewEggOrderItems','id','fk_OrderNumber');
        }
        public function orderStatus(){
            return $this->belongsTo('App\OrderStatus','order_status_id','id');
        }
}
