<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Role::create(['role'=>'Administrator']);
        Role::create(['role'=>'Sales Department']);
        Role::create(['role'=>'Purchase Department']);
    }
}
