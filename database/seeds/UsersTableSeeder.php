<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // User::truncate();
        User::create([
            'name'=>'Administrator',
            'email'=>'admin@admin.com',
            'password'=>'admin',
            'plain_password'=>'admin',
            'role_id'=>1,
            ]);
    }
}
