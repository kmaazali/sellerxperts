<?php

use Illuminate\Database\Seeder;
use App\OrderStatus;

class OrderStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderStatus::create(['status'=>'NIL']);
        OrderStatus::create(['status'=>'Order Created']);
        OrderStatus::create(['status'=>'Order Processed']);
        OrderStatus::create(['status'=>'Order Shipped']);
        OrderStatus::create(['status'=>'Order Delivered']);
        OrderStatus::create(['status'=>'Order Exception']);
    }
}
