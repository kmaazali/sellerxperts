<?php

use App\UserAccess;
use Illuminate\Database\Seeder;

class UsersAccessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserAccess::create([
            'user_id'=> 1
        ]);
    }
}
