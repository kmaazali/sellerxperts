<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNeweggOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newegg_order_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('fk_OrderNumber')->unsigned()->index();
            $table->foreign('fk_OrderNumber')->references('id')->on('newegg_orders');
            $table->string('SellerPartNumber')->nullable();
            $table->string('NeweggItemNumber')->nullable();
            $table->string('MfrPartNumber')->nullable();
            $table->string('UPCCode')->nullable();
            $table->string('Description')->nullable();
            $table->string('OrderedQty')->nullable();
            $table->string('ShippedQty')->nullable();
            $table->string('UnitShippingCharge')->nullable();
            $table->string('UnitPrice')->nullable();
            $table->string('ExtendUnitPrice')->nullable();
            $table->string('ExtendShippingCharge')->nullable();
            $table->string('ExtendSalesTax')->nullable();
            $table->string('ExtendVAT')->nullable();
            $table->string('ExtendDuty')->nullable();
            $table->string('Status')->nullable();
            $table->string('StatusDescription')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newegg_order_items');
    }
}
