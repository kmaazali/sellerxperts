<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNeweggOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('newegg_orders', function (Blueprint $table) {
            $table->integer('id')->unsigned()->unique();
            $table->primary('id');
            //$table->integer('OrderNumber')->unique();
            $table->string('CustomerPONumber')->nullable();
            $table->string('SellerCustomerNumber')->nullable();
            $table->string('SellerOrderNumber')->nullable()->unique();
            $table->dateTime('OrderDate');
            $table->string('OrderStatus')->nullable();
            $table->string('IsAutoVoid')->nullable();
            $table->string('OrderStatusDescription')->nullable();
            $table->string('InvoiceNumber')->nullable();
            $table->string('CustomerName');
            $table->string('CustomerPhoneNumber');
            $table->string('CustomerEmailAddress');
            $table->string('ShipToAddress1')->nullable();
            $table->string('ShipToAddress2')->nullable();
            $table->string('ShipToCityName')->nullable();
            $table->string('ShipToStateCode')->nullable();
            $table->string('ShipToZipCode')->nullable();
            $table->string('ShipToCountryCode')->nullable();
            $table->string('ShipService')->nullable();
            $table->string('ShipToFirstName')->nullable();
            $table->string('ShipToLastName')->nullable();
            $table->string('ShipToCompany')->nullable();
            $table->string('OrderItemAmount')->nullable();
            $table->string('ShippingAmount')->nullable();
            $table->string('DiscountAmount')->nullable();
            $table->string('RefundAmount')->nullable();
            $table->string('SalesTax')->nullable();
            $table->string('VATTotal')->nullable();
            $table->string('DutyTotal')->nullable();
            $table->string('OrderTotalAmount')->nullable();
            $table->string('OrderQty')->nullable();
            $table->string('SalesChannel')->nullable();
            $table->string('FulfillmentOption')->nullable();
            $table->string('channel');
            $table->bigInteger('order_status_id')->unsigned()->index();
            $table->foreign('order_status_id')->references('id')->on('order_status')->onDelete('cascade');
            // $table->unsignedBigInteger('order_status_id');
            $table->string('resource_fees')->nullable();
            $table->string('resource_fees_unit')->nullable();
            $table->string('payment_fees')->nullable();
            $table->string('payment_fees_unit')->nullable();
            $table->string('earning')->nullable();
            $table->string('purchase_price')->nullable();
            $table->string('profit')->nullable();
            $table->string('note')->nullable();
            $table->string('updated_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('newegg_orders');
    }
}
