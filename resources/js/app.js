import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import App from './App.vue';
import Login from './components/Login.vue';
import Index from './components/Index.vue';
import Users from './components/Users.vue';
import AllOrders from './components/AllOrders.vue';
import StatusOrders from './components/StatusOrders.vue';
import Fees from './components/Fees.vue';
import OrderDetail from './components/OrderDetail.vue';
import Customers from './components/Customers.vue';
import Products from './components/Products.vue';
import AllProcessed from './components/AllProcessed.vue';
import Unprocessed from './components/Unprocessed.vue';

import { ClientTable } from 'vue-tables-2';
Vue.use(ClientTable);
// import '../node_modules/bootstrap/dist/css/bootstrap.min.css'

// import Bootstrap from '../../public/js/bootstrap.min.js';
// window.$ = jQuery;

// Vue.component('pulse-loader', require('vue-spinner/src/PulseLoader.vue'));
Vue.component('pagination', require('laravel-vue-pagination'));

Vue.use(VueRouter);
Vue.use(VueAxios, axios);
const token=localStorage.getItem("token");
axios.defaults.baseURL = 'http://127.0.0.1:8000/api/';
axios.defaults.path = 'http://127.0.0.1:8000/#/order-detail/'
axios.defaults.headers.common['Authorization'] = 'Bearer '+token;
const router = new VueRouter({
    routes: [{
        path: '/',
        name: 'login',
        component: Login,
        meta: {
            title: 'Login - SellerXperts',
        }
    },
        {
            path: '/index',
            name: 'index',
            component: Index,
            meta: {
                title: 'Index - SellerXperts',
            }
        },
        {
            path: '/users',
            name: 'users',
            component: Users,
            meta: {
                title: 'Users - SellerXperts',
            }
        },
        {
            path: '/all-orders',
            name: 'all-orders',
            component: AllOrders,
            meta: {
                title: 'All Orders - SellerXperts',
            }
        },
        {
            path: '/status-orders',
            name: 'status-orders',
            component: StatusOrders,
            meta: {
                title: 'Status Orders - SellerXperts',
            }
        },
        {
            path: '/fees',
            name: 'fees',
            component: Fees,
            meta: {
                title: 'Fee - SellerXperts',
            }
        },
        {
            path: '/order-detail',
            name: 'order-detail',
            component: OrderDetail,
            meta: {
                title: 'Order Detail - SellerXperts',
            }
        },
        {
            path: '/customers',
            name: 'customers',
            component: Customers,
            meta: {
                title: 'Customers - SellerXperts',
            }
        },
        {
            path: '/products',
            name: 'products',
            component: Products,
            meta: {
                title: 'Products - SellerXperts',
            }
        },
        {
            path: '/all-processed',
            name: 'all-processed',
            component: AllProcessed,
            meta: {
                title: 'All Processed - SellerXperts',
            }
        },
        {
            path: '/unprocessed',
            name: 'unprocessed',
            component: Unprocessed,
            meta: {
                title: 'Unprocessed - SellerXperts',
            }
        },
    ]
});
Vue.router = router
Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
});
App.router = Vue.router
new Vue(App).$mount('#app');


// This callback runs before every route change, including on page load.
router.beforeEach((to, from, next) => {
    // This goes through the matched routes from last to first, finding the closest route with a title.
    // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

    // Find the nearest route element with meta tags.
    const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
    const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

    // If a route with a title was found, set the document (page) title to that value.
    if(nearestWithTitle) document.title = nearestWithTitle.meta.title;

    // Remove any stale meta tags from the document using the key attribute we set below.
    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

    // Skip rendering meta tags if there are none.
    if(!nearestWithMeta) return next();

    // Turn the meta tag definitions into actual elements in the head.
    nearestWithMeta.meta.metaTags.map(tagDef => {
        const tag = document.createElement('meta');

        Object.keys(tagDef).forEach(key => {
            tag.setAttribute(key, tagDef[key]);
        });

        // We use this to track which meta tags we create, so we don't interfere with other ones.
        tag.setAttribute('data-vue-router-controlled', '');

        return tag;
    })
    // Add the meta tags to the document head.
        .forEach(tag => document.head.appendChild(tag));

    next();
});
