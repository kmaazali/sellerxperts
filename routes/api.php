<?php


use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');

        $api->post('logout', 'App\\Api\\V1\\Controllers\\LogoutController@logout');
        $api->post('refresh', 'App\\Api\\V1\\Controllers\\RefreshController@refresh');
        $api->get('me', 'App\\Api\\V1\\Controllers\\UserController@me');
    });

    
    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        Route::prefix('admin')->group(function() {

            Route::get('status','AdminController@getOrderStatuses');
            Route::get('test','AdminController@test');
            Route::get('getallorders','AdminController@getAllOrders');
            Route::post('getallorders/po','AdminController@getAllOrdersPO');
            Route::get('get/order/{id}','AdminController@getSingleOrder');
            Route::post('ordersbydate','AdminController@getOrders');
            Route::post('adduser','AdminController@addUser');
            Route::get('delete/user/{id}','AdminController@deleteUser');
            Route::get('get/all/users','AdminController@getUsers');
            Route::post('edit/user','AdminController@editUser');
            Route::get('get/user/{id}','AdminController@getUser');
            Route::post('change/password','AdminController@changePassword');
            Route::get('get/status','AdminController@getStatus');
            Route::get('get/dd-status','AdminController@getStatusDropdown');

            Route::get('fees','AdminController@getFees');
            Route::get('get/fee/{id}','AdminController@getSingleFee');
            Route::post('add/fee','AdminController@addFees');
            Route::post('edit/fee','AdminController@editFees');
            Route::get('delete/fee/{id}','AdminController@deleteFees');
            Route::get('customers','AdminController@getCustomers');
            Route::get('customers/{term}','AdminController@search');

            Route::get('products','AdminController@getProducts');
            Route::get('get/role/dropdown', 'AdminController@getRoleDropdown');
            Route::post('edit/order/status', 'AdminController@editOrderStatus');
            Route::get('update/orderstatus/{order}','AdminController@updateStatus');    
            Route::get('update/po/orderstatus/{order}','AdminController@updatePOStatus');    
            Route::post('update/resourcefee','AdminController@updateResourceFee');
            Route::post('update/paymentfee','AdminController@updatePaymentFee');
            Route::post('update/puchase/price','AdminController@updatePurchasePrice');
            Route::post('update/note','AdminController@updateNote');
            
            Route::post('user/access','UserAccessController@userAccess');
            Route::get('user/access/{id}','UserAccessController@getUserAccess');
    

        });
        // Route::prefix('sales')->middleware(['jwt.auth','sales'])->group(function() {
        //     Route::get('test','AdminController@test');
        // });
    });


//    $api->group(['middleware' => 'jwt.auth','admin'], function(Router $api) {
//        $api->get('protected', function() {
//            return response()->json([
//                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
//            ]);
//        });
//
//        $api->get('refresh', [
//            'middleware' => 'jwt.refresh',
//            function() {
//                return response()->json([
//                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
//                ]);
//            }
//        ]);
//    });


});
