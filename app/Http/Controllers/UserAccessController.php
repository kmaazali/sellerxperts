<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserAccess;

class UserAccessController extends Controller
{
    public function userAccess(Request $request){

        $ua = UserAccess::where('user_id', $request['user_id'])->first();
        // dd($ua);
        $ua->customers = $request['customers'];
        $ua->fees = $request['fees'];
        $ua->orders = $request['orders'];
        $ua->products = $request['products'];
        $ua->users = $request['users'];
        $ua->save();
        return response()->json(['success'=>true,'User Access'=>$ua],200);
    }

    public function getUserAccess($id){

        $ua = UserAccess::where('user_id', $id)->get();
        // dd($ua);
        return response()->json(['success'=>true,'list'=>$ua],200);
    }
}
