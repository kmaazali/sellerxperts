<?php

namespace App\Http\Controllers;

use App\NewEggOrderItems;
use App\NewEggOrders;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AppController extends Controller
{
    //
    public function crontest()
    {
        // $today = Carbon::today()->toDateString();
        $today="2019-09-03";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.newegg.com/marketplace/ordermgmt/order/orderinfo?sellerid=AH8W&version=308');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n    \"OperationType\": \"GetOrderInfoRequest\",\n    \"RequestBody\": {\n        \"PageIndex\": \"1\",\n        \"PageSize\": \"500\",\n        \"RequestCriteria\": {\n        \t\"OrderDateFrom\": \"" . $today . " 00:00:00\",\n        \t\"OrderDateTo\": \"" . $today . " 23:59:59\"\n        }\n    }\n}");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');


        $headers = array();
        $headers[] = 'Accept: application/json';
        //$headers[] = 'Accept-Encoding: gzip, deflate';
        $headers[] = 'Authorization: 7a7a91536f744775b7d7d3f0e45168f5';
        //$headers[] = 'Cache-Control: no-cache';
        //$headers[] = 'Connection: keep-alive';
        //$headers[] = 'Content-Length: 166';
        $headers[] = 'Content-Type: application/json';
        //$headers[] = 'Cookie: NSC_JOlppyvtcp5oabvbw2fkaqcyk42rxbm=14b5a3d9bef4addb841b701f2f68e94f9ebf858661289eb97a1ad5416b59ca597445076c';
        //$headers[] = 'Host: api.newegg.com';
        //$headers[] = 'Postman-Token: 914cc07d-e5e7-47c0-b192-a54e2ec3d3ea,59a79864-c366-4e86-84e9-dab33133b5e0';
        $headers[] = 'Secretkey: be14db67-adfb-417b-a806-e3216c1755d0';
        //$headers[] = 'User-Agent: PostmanRuntime/7.15.2';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $result = json_decode($result);

        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }

        curl_close($ch);
        foreach ($result->ResponseBody->OrderInfoList as $order) {
            $count = NewEggOrders::count();
            $count = $count + 1;
            $neweggorder = new NewEggOrders();
            $neweggorder->id = $order->OrderNumber;
            // $neweggorder->SellerOrderNumber = "SO-". $count;
            $neweggorder->CustomerPONumber = $order->CustomerPONumber;
            $neweggorder->SellerCustomerNumber = $order->SellerCustomerNumber;
            $neweggorder->OrderDate = Carbon::parse($order->OrderDate);
            $neweggorder->OrderStatusDescription = $order->OrderStatusDescription;
            $neweggorder->CustomerName = $order->CustomerName;
            $neweggorder->CustomerPhoneNumber = $order->CustomerPhoneNumber;
            $neweggorder->CustomerEmailAddress = $order->CustomerEmailAddress;
            $neweggorder->ShipToAddress1 = $order->ShipToAddress1;
            $neweggorder->ShipToAddress2 = $order->ShipToAddress2;
            $neweggorder->ShipToCityName = $order->ShipToCityName;
            $neweggorder->ShipToStateCode = $order->ShipToStateCode;
            $neweggorder->ShipToZipCode = $order->ShipToZipCode;
            $neweggorder->ShipToCountryCode = $order->ShipToCountryCode;
            $neweggorder->ShipService = $order->ShipService;
            $neweggorder->ShipToFirstName = $order->ShipToFirstName;
            $neweggorder->ShipToLastName = $order->ShipToLastName;
            $neweggorder->ShipToCompany = $order->ShipToCompany;
            $neweggorder->OrderItemAmount = $order->OrderItemAmount;
            $neweggorder->ShippingAmount = $order->ShippingAmount;
            $neweggorder->DiscountAmount = $order->DiscountAmount;
            $neweggorder->RefundAmount = $order->RefundAmount;
            $neweggorder->SalesTax = $order->SalesTax;
            $neweggorder->VATTotal = $order->VATTotal;
            $neweggorder->DutyTotal = $order->DutyTotal;
            $neweggorder->OrderTotalAmount = $order->OrderTotalAmount;
            $neweggorder->OrderQty = $order->OrderQty;
            $neweggorder->channel="B2C";
            $neweggorder->order_status_id=1;
            $neweggorder->save();
            foreach ($order->ItemInfoList as $orderItem) {
                $neweggorderitem = new NewEggOrderItems();
                $neweggorderitem->fk_OrderNumber = $order->OrderNumber;
                $neweggorderitem->SellerPartNumber = $orderItem->SellerPartNumber;
                $neweggorderitem->NeweggItemNumber = $orderItem->NeweggItemNumber;
                $neweggorderitem->MfrPartNumber = $orderItem->MfrPartNumber;
                $neweggorderitem->UPCCode = $orderItem->UPCCode;
                $neweggorderitem->Description = $orderItem->Description;
                $neweggorderitem->OrderedQty = $orderItem->OrderedQty;
                $neweggorderitem->ShippedQty = $orderItem->ShippedQty;
                $neweggorderitem->UnitPrice = $orderItem->UnitPrice;
                $neweggorderitem->ExtendUnitPrice = $orderItem->ExtendUnitPrice;
                $neweggorderitem->ExtendShippingCharge = $orderItem->ExtendShippingCharge;
                $neweggorderitem->ExtendSalesTax = $orderItem->ExtendSalesTax;
                $neweggorderitem->ExtendVAT = $orderItem->ExtendVAT;
                $neweggorderitem->ExtendDuty = $orderItem->ExtendDuty;
                $neweggorderitem->Status = $orderItem->Status;
                $neweggorderitem->StatusDescription = $orderItem->StatusDescription;
                $neweggorderitem->save();
            }

        }


        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.newegg.com/marketplace/b2b/ordermgmt/order/orderinfo?sellerid=V1VD&version=308');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n    \"OperationType\": \"GetOrderInfoRequest\",\n    \"RequestBody\": {\n        \"PageIndex\": \"1\",\n        \"PageSize\": \"500\",\n        \"RequestCriteria\": {\n        \t\"OrderDateFrom\": \"" . $today . " 00:00:00\",\n        \t\"OrderDateTo\": \"" . $today . " 23:59:59\"\n        }\n    }\n}");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');


        $headers = array();
        $headers[] = 'Accept: application/json';
        //$headers[] = 'Accept-Encoding: gzip, deflate';
        $headers[] = 'Authorization: 7a7a91536f744775b7d7d3f0e45168f5';
        //$headers[] = 'Cache-Control: no-cache';
        //$headers[] = 'Connection: keep-alive';
        //$headers[] = 'Content-Length: 166';
        $headers[] = 'Content-Type: application/json';
        //$headers[] = 'Cookie: NSC_JOlppyvtcp5oabvbw2fkaqcyk42rxbm=14b5a3d9bef4addb841b701f2f68e94f9ebf858661289eb97a1ad5416b59ca597445076c';
        //$headers[] = 'Host: api.newegg.com';
        //$headers[] = 'Postman-Token: 914cc07d-e5e7-47c0-b192-a54e2ec3d3ea,59a79864-c366-4e86-84e9-dab33133b5e0';
        $headers[] = 'Secretkey: d7291799-1e55-4ba0-9db3-bd50a6c8a538';
        //$headers[] = 'User-Agent: PostmanRuntime/7.15.2';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $result = json_decode($result);

        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        //return view('GetNewEggOrdersB2B',['orders'=>$result]);


        foreach ($result->ResponseBody->OrderInfoList as $order) {
            $count = NewEggOrders::count();
            $count = $count + 1;
            $b2borders = new NewEggOrders();
            $b2borders->id = $order->OrderNumber;
            $b2borders->SellerOrderNumber = "SO-".$count;
            $b2borders->CustomerPONumber = $order->CustomerPONumber;
            $b2borders->SellerCustomerNumber = $order->SellerCustomerNumber;
            $b2borders->InvoiceNumber = $order->InvoiceNumber;
            $b2borders->OrderDate = Carbon::parse($order->OrderDate);
            $b2borders->OrderStatus = $order->OrderStatus;
            $b2borders->OrderStatusDescription = $order->OrderStatusDescription;
            $b2borders->CustomerName = $order->CustomerName;
            $b2borders->CustomerPhoneNumber = $order->CustomerPhoneNumber;
            $b2borders->CustomerEmailAddress = $order->CustomerEmailAddress;
            $b2borders->ShipToAddress1 = $order->ShipToAddress1;
            $b2borders->ShipToAddress2 = $order->ShipToAddress2;
            $b2borders->ShipToCityName = $order->ShipToCityName;
            $b2borders->ShipToStateCode = $order->ShipToStateCode;
            $b2borders->ShipToZipCode = $order->ShipToZipCode;
            $b2borders->ShipToCountryCode = $order->ShipToCountryCode;
            $b2borders->ShipService = $order->ShipService;
            $b2borders->ShipToFirstName = $order->ShipToFirstName;
            $b2borders->ShipToLastName = $order->ShipToLastName;
            $b2borders->ShipToCompany = $order->ShipToCompany;
            $b2borders->OrderItemAmount = $order->OrderItemAmount;
            $b2borders->ShippingAmount = $order->ShippingAmount;
            $b2borders->DiscountAmount = $order->DiscountAmount;
            $b2borders->RefundAmount = $order->RefundAmount;
            $b2borders->OrderTotalAmount = $order->OrderTotalAmount;
            $b2borders->OrderQty = $order->OrderQty;
            $b2borders->IsAutoVoid = $order->IsAutoVoid;
            $b2borders->SalesChannel = $order->SalesChannel;
            $b2borders->FulfillmentOption = $order->FulfillmentOption;
            $b2borders->channel="B2B";
            $b2borders->order_status_id=1;
            $b2borders->save();
            foreach ($order->ItemInfoList as $orderItem) {
                $neweggorderitemb2b = new NewEggOrderItems();
                $neweggorderitemb2b->fk_OrderNumber = $order->OrderNumber;
                $neweggorderitemb2b->SellerPartNumber = $orderItem->SellerPartNumber;
                $neweggorderitemb2b->NeweggItemNumber = $orderItem->NeweggItemNumber;
                $neweggorderitemb2b->MfrPartNumber = $orderItem->MfrPartNumber;
                $neweggorderitemb2b->UPCCode = $orderItem->UPCCode;
                $neweggorderitemb2b->Description = $orderItem->Description;
                $neweggorderitemb2b->OrderedQty = $orderItem->OrderedQty;
                $neweggorderitemb2b->ShippedQty = $orderItem->ShippedQty;
                $neweggorderitemb2b->UnitPrice = $orderItem->UnitPrice;
                $neweggorderitemb2b->ExtendUnitPrice = $orderItem->ExtendUnitPrice;
                $neweggorderitemb2b->UnitShippingCharge = $orderItem->UnitShippingCharge;
                $neweggorderitemb2b->ExtendShippingCharge = $orderItem->ExtendShippingCharge;
                $neweggorderitemb2b->Status = $orderItem->Status;
                $neweggorderitemb2b->StatusDescription = $orderItem->StatusDescription;
                $neweggorderitemb2b->save();
            }
        }
        echo "Done";
    }


}
