<?php

namespace App\Http\Controllers;

use App\Fees;
use App\OrderStatus;
use App\NewEggOrderItems;
use App\NewEggOrders;
use App\UserAccess;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    //
    public function getAllOrders(){
        $orders=NewEggOrders::with('orderItem','orderStatus')->where('order_status_id', '2')->get();
        return response()->json(['success'=>true,'orders'=>$orders],200);
    }

    public function getAllOrdersPO(Request $request){
        $orders=NewEggOrders::with('orderItem','orderStatus')->where('order_status_id', '2')->paginate($request['no']);
        return response()->json(['success'=>true,'orders'=>$orders],200);
    }

    public function getOrders(Request $request){
            $orders = NewEggOrders::where('order_status_id',1)->whereNull('SellerOrderNumber')->with('orderItem','orderStatus')->paginate($request['no']);
            // dd($orders);
            return response()->json(['success'=>true,'orders'=>$orders],200);
        
    }

    public function getSingleOrder($id){
        $order=NewEggOrders::where('SellerOrderNumber',$id)->with('orderItem')->get();
        return response()->json(['success'=>true,'order'=>$order],200);
    }

    public function test(){
        return response()->json(['success'=>true,'message'=>'done']);
    }

    public function addUser(Request $request){
        $user=new User();
        $user->name=$request['name'];
        $user->email=$request['email'];
        $user->password=$request['password'];
        $user->plain_password=$request['password'];
        $user->role_id=$request['role_id'];
        $user->save();
        $useraccess=new UserAccess();
        $useraccess->user_id=$user->id;
        // $useraccess->views=json_encode(['orders'=>'disabled','fees'=>'disabled','customers'=>'disabled','users'=>'disabled','products'=>'disabled']);
        $useraccess->save();
        return response()->json(['success'=>true,'message'=>'User Added Successfully'],200);
    }

    public function deleteUser($id){
        $user=User::where('id',$id)->delete();
        return response()->json(['success'=>true,'message'=>'User Deleted Successfully'],200);
    }

    public function getUsers(){
        $users=User::with('role')->paginate(10);
        return response()->json(['success'=>true,'users'=>$users],200);
    }

    public function editUser(Request $request){
        $user=User::where('id',$request['id'])->first();
        $user->name=$request['name'];
        $user->email=$request['email'];
        $user->role_id=$request['role_id'];
        $user->save();
        return response()->json(['success'=>true,'message'=>'User Updated Successfully'],200);
    }

    public function getUser($id){
        $user=User::where('id',$id)->with('role')->first();
        return response()->json(['success'=>true,'user'=>$user],200);
    }

    public function changePassword(Request $request){
        $user=User::where('id',$request['id'])->first();
        $user->password=$request['password'];
        $user->plain_password=$request['password'];
        $user->save();
        return response()->json(['success'=>true,'message'=>'User Updated Successfully'],200);
    }

    public function getStatus(){
        // $status=DB::table('newegg_orders')->select(DB::raw('DISTINCT order_status_id as status'))->get();
        $status = DB::table('order_status')
        ->select('order_status.id','status')
        ->join('newegg_orders', 'order_status.id', '=', 'newegg_orders.order_status_id')
        ->groupBy('order_status.status','order_status.id')
        ->get();
        return response()->json(['success'=>true,'status'=>$status]);
    }

    public function getStatusDropdown(){
        $status = DB::table('order_status')
        ->select('id','status')->get();
        return response()->json(['success'=>true, 'status'=>$status]);
    }

    public function getRoleDropdown(){
        $role = DB::table('roles')
        ->select('id','role')->get();
        // dd($role);
        return response()->json(['success'=>true, 'role'=>$role]);
    }

    public function getFees(){
        $fees=Fees::paginate(2);
        return response()->json(['success'=>true,'fees'=>$fees],200);
    }

    public function getSingleFee($id){
        $fees=Fees::where('id',$id)->first();
        return response()->json(['success'=>true,'fees'=>$fees],200);
    }

    public function addFees(Request $request){
        $fee=new Fees();
        $fee->category=$request['category'];
        $fee->fee=$request['fee'];
        $fee->save();
        return response()->json(['success'=>true,'message'=>'Fees Added Successfully'],200);
    }

    public function editFees(Request $request){
        $fee=Fees::where('id',$request['id'])->first();
        $fee->category=$request['category'];
        $fee->fee=$request['fee'];
        $fee->save();
        return response()->json(['success'=>true,'message'=>'Fees Edited Successfully'],200);
    }

    public function deleteFees($id){
        Fees::where('id',$id)->delete();
        return response()->json(['success'=>true,'message'=>'Fees Deleted Successfully'],200);
    }

    public function getCustomers(){
        $customers=DB::table('newegg_orders')->select(DB::raw('DISTINCT CustomerName,CustomerPhoneNumber,CustomerEmailAddress,ShipToAddress1,ShipToAddress2,ShipToCityName,ShipToStateCode,ShipToZipCode'))->get();
        return response()->json(['success'=>true,'customers'=>$customers],200);
    }

    public function search($term){
        $customers=DB::table('newegg_orders')->select(DB::raw('DISTINCT CustomerName,CustomerPhoneNumber,CustomerEmailAddress WHERE CustomerName LIKE %'.$term.'% or CustomerPhoneNumber LIKE %'.$term.'% or CustomerEmailAddress LIKE %'.$term.'%'))->paginate(10);
    }

    public function getProducts(){
        $products=DB::table('newegg_order_items')->join('newegg_orders','newegg_orders.id','=','newegg_order_items.fk_OrderNumber')->get();
        
        //$products=$products->unique('Description');
        //$resultArray = json_decode(json_encode($products), true);
        
        return response()->json(['success'=>true,'products'=>$products],200);
    }

    public function editOrderStatus(Request $request){
        $Order = NewEggOrders::where('id',$request['id'])->first();
        $Order->order_status_id = $request['order_status_id'];
        $Order->save();
        return response()->json(['success'=>true,'message'=>'Order Updated Successfully'],200);
    }

    public function getOrderStatuses(){
        $status=OrderStatus::get();
        return response()->json(['success'=>true,'status'=>$status],200);
    }

    public function updateStatus($order){
        $Order = NewEggOrders::where('id',$order)->first();
        // dd($Order);
        $price = $Order->OrderTotalAmount;
        $resource_fee = $Order->resource_fees;
        $payment_fees = $Order->payment_fees;
        //dd($resource_fee.' '.$payment_fees.' '.$price);
        // dd($date);
        $rand = mt_rand(100000, 999999);
        $date = date("d-m-Y");
        // dd((float)$price-(float)$payment_fees-(float)$resource_fee);
        $Order->earning = number_format((float)$price-((float)$payment_fees+(float)$resource_fee), 2);
        $Order->updated_date = $date;
        $Order->SellerOrderNumber = 'SO-'.$rand;
        $Order->CustomerPONumber = 'PO-'.$rand;
        $Order->order_status_id = 2;
        $Order->save();
        // dd($Order);
        return response()->json(['success'=>true,'message'=>'Order Updated Successfully'],200);
    }

    public function updatePOStatus($order){
        $Order = NewEggOrders::where('id',$order)->first();

        $Order->order_status_id = 3;
        $Order->save();
        // dd($Order);
        return response()->json(['success'=>true,'message'=>'Order Updated Successfully'],200);
    }

    public function updateResourceFee(Request $request){
        $order=NewEggOrders::where('id',$request['id'])->first();
        $order->resource_fees=$request['resource_fees'];
        $order->resource_fees_unit=$request['resource_fees_unit'];
        //$order->payment_fees=$request['payment_fees'];
        $order->save();
        return response()->json(['success'=>true,'message'=>'Resource Fee Updated'],200);
    }

    public function updatePaymentFee(Request $request){
        $order=NewEggOrders::where('id',$request['id'])->first();
        $order->payment_fees=$request['payment_fees'];
        $order->payment_fees_unit=$request['payment_fees_unit'];
        //$order->payment_fees=$request['payment_fees'];
        $order->save();
        return response()->json(['success'=>true,'message'=>'Payment Fee Updated'],200);
    }

    public function updatePurchasePrice(Request $request){
        $order=NewEggOrders::where('id',$request['id'])->first();
        $order->purchase_price=$request['purchase_price'];
        $order->profit = number_format($order->earning-$request['purchase_price'], 2);
        // dd($order);
        //$order->payment_fees=$request['payment_fees'];
        $order->save();
        return response()->json(['success'=>true,'message'=>'Payment Fee Updated'],200);
    }

    public function updateNote(Request $request){
        $order=NewEggOrders::where('id',$request['id'])->first();
        $order->note=$request['note'];
        
        $order->save();
        return response()->json(['success'=>true,'message'=>'Payment Fee Updated'],200);
    }

}
