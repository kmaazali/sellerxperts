<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewEggOrderItems extends Model
{
    //
    protected $table='newegg_order_items';

    protected $fillable=['fk_OrderNumber',
    'SellerPartNumber',
    'NeweggItemNumber',
    'MfrPartNumber',
    'UPCCode',
    'Description',
    'OrderedQty',
    'ShippedQty',
    'UnitPrice',
    'ExtendUnitPrice',
    'ExtendShippingCharge',
    'ExtendSalesTax',
    'ExtendVAT',
    'ExtendDuty',
    'Status',
    'StatusDescription'];

    public function order(){
        return $this->belongsTo('App\NewEggOrders','fk_OrderNumber','id');
    }
}
