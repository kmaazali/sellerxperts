<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class NewEggOrders extends Model
{
    //  
    protected $table='newegg_orders';

    protected $fillable=['SellerOrderNumber',
        'CustomerPONumber',
        'SellerCustomerNumber',
        'OrderDate',
        'OrderStatusDescription',
        'CustomerName',
        'CustomerPhoneNumber',
        'CustomerEmailAddress',
        'ShipToAddress1',
        'ShipToAddress2',
        'ShipToCityName',
        'ShipToStateCode',
        'ShipToZipCode',
        'ShipToCountryCode',
        'ShipService',
        'ShipToFirstName',
        'ShipToLastName',
        'ShipToCompany',
        'OrderItemAmount',
        'ShippingAmount',
        'DiscountAmount',
        'RefundAmount',
        'SalesTax',
        'VATTotal',
        'DutyTotal',
        'OrderTotalAmount',
        'OrderQty',
        'order_status_id',
        'resource_fees',
        'payment_fees',
        'earning',
        'purchase_price',
        'profit',
        'note',
        'updated_date',
];

        protected $casts = [
            'OrderDate' => 'date:Y-m-d',
        ];
        
        public function orderItem(){
            return $this->belongsTo('App\NewEggOrderItems','id','fk_OrderNumber');
        }
        public function orderStatus(){
            return $this->belongsTo('App\OrderStatus','order_status_id','id');
        }

}
