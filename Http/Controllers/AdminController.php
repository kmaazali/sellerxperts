<?php

namespace App\Http\Controllers;

use App\Fees;
use App\NewEggOrderItems;
use App\NewEggOrders;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    //
    public function getAllOrders(){
        $orders=NewEggOrders::with('orderItem','orderStatus')->paginate(10);
        return response()->json(['success'=>true,'orders'=>$orders],200);
    }

    public function getOrders(Request $request){
        if($request->has('date')){
            $orders=NewEggOrders::where('OrderDate','like', '%' .$request['date'].'%')->with('orderItem','orderStatus')->paginate(10);
            return response()->json(['success'=>true,'orders'=>$orders],200);
        }
        else{
            return response()->json(['success'=>false,'message'=>'Please Select A Valid Date'],402);
        }
    }

    public function getSingleOrder($id){
        $order=NewEggOrders::where('id',$id)->with('orderItem')->get();
        return response()->json(['success'=>true,'order'=>$order],200);
    }

    public function test(){
        return response()->json(['success'=>true,'message'=>'done']);
    }

    public function addUser(Request $request){
        $user=new User();
        $user->name=$request['name'];
        $user->email=$request['email'];
        $user->password=$request['password'];
        $user->role_id=$request['role_id'];
        $user->save();
        return response()->json(['success'=>true,'message'=>'User Added Successfully'],200);
    }

    public function deleteUser($id){
        $user=User::where('id',$id)->delete();
        return response()->json(['success'=>true,'message'=>'User Deleted Successfully'],200);
    }

    public function getUsers(){
        $users=User::with('role')->paginate(1);
        return response()->json(['success'=>true,'users'=>$users],200);
    }

    public function editUser(Request $request){
        $user=User::where('id',$request['id'])->first();
        $user->name=$request['name'];
        $user->email=$request['email'];
        $user->role_id=$request['role_id'];
        $user->save();
        return response()->json(['success'=>true,'message'=>'User Updated Successfully'],200);
    }

    public function getUser($id){
        $user=User::where('id',$id)->with('role')->first();
        return response()->json(['success'=>true,'user'=>$user],200);
    }

    public function changePassword(Request $request){
        $user=User::where('id',$request['id'])->first();
        $user->password=$request['password'];
        $user->save();
        return response()->json(['success'=>true,'message'=>'User Updated Successfully'],200);
    }

    public function getDates(){
        $dates=DB::table('newegg_orders')->select(DB::raw('DISTINCT DATE(OrderDate) as date'))->get();
        return response()->json(['success'=>true,'dates'=>$dates]);
    }

    public function getFees(){
        $fees=Fees::paginate(2);
        return response()->json(['success'=>true,'fees'=>$fees],200);
    }

    public function getSingleFee($id){
        $fees=Fees::where('id',$id)->first();
        return response()->json(['success'=>true,'fees'=>$fees],200);
    }

    public function addFees(Request $request){
        $fee=new Fees();
        $fee->category=$request['category'];
        $fee->fee=$request['fee'];
        $fee->save();
        return response()->json(['success'=>true,'message'=>'Fees Added Successfully'],200);
    }

    public function editFees(Request $request){
        $fee=Fees::where('id',$request['id'])->first();
        $fee->category=$request['category'];
        $fee->fee=$request['fee'];
        $fee->save();
        return response()->json(['success'=>true,'message'=>'Fees Edited Successfully'],200);
    }

    public function deleteFees($id){
        Fees::where('id',$id)->delete();
        return response()->json(['success'=>true,'message'=>'Fees Deleted Successfully'],200);
    }

}
