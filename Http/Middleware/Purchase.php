<?php

namespace App\Http\Middleware;

use Closure;

class Purchase
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->role_id == 3){
            return $next($request);
          }
        return response()->json(['success'=>false,'message'=>'You Are Not Allowed To Access This Area']);
    }
}
