<?php

namespace App\Http\Middleware;

use Closure;

class Sales
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->role_id == 2){
            return $next($request);
          }
        return response()->json(['success'=>false,'message'=>'You Are Not Allowed To Access This Area']);
    }
}
